﻿var kevinappdevApp = angular.module('kevinappdevApp',
    [
    'ui.router'
    ]);

kevinappdevApp.constant('about', {
    ver: '0.0.1',
    copyrightYear: '2017'
})

kevinappdevApp.config(function ($urlRouterProvider, $stateProvider) {

    $stateProvider.state('app.Hobbies', {
        url: '/Hobbies',
        template: "<h3>Hobbies</h3>",
        controller: function () { }
    })
    $stateProvider.state('app.About', {
        url: '/About',
        template: "<h3>About</h3>",
        controller: function () { }
    })
    $stateProvider.state('app.FullStack', {
        url: '/FullStack',
        template: "<h3>Full Stack</h3>",
        controller: function () { }
    })
    $stateProvider.state('app.AngularJS', {
        url: '/AngularJS',
        template: "<h2>This site was created using Angular JS</h2><p>Click <a href='http://kevinappdev.com' >here</a> to view the Microsoft MVC version.</p>",
        controller: function () { }
    })

    $urlRouterProvider.otherwise('/app/Home');
});

