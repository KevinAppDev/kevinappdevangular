﻿
angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.FeaturedSkills', {
            url: '/FeaturedSkills',
            templateUrl: 'components/featuredskills/featuredSkills.template.html',
            controller: 'featuredSkillsCtrl'
        })
    })
    .controller('featuredSkillsCtrl', [function () {

    }]);
