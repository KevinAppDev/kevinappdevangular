﻿
angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Experience', {
            url: '/Experience',
            templateUrl: "components/experience/experience.template.html",
            controller: 'experienceController'
        })
    })
    .controller('experienceController', [function () {

}]);