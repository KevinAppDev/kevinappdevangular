﻿angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Experience.HeritageAuctionGalleries', {
            url: '/HeritageAuctionGalleries',
            views: {
                'viewHeritageAuctionGalleries': {
                    templateUrl: 'components/experience/templates/heritageAuctionGalleries.template.html',
                    controller: 'heritageAuctionGalleriesController'
                }
            }

        })
    })
    .controller('heritageAuctionGalleriesController', [function () { }]);