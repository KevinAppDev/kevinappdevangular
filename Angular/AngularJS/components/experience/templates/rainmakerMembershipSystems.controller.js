﻿angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Experience.RainmakerMembershipSystems', {
            url: '/RainmakerMembershipSystems',
            views: {
                'viewRainmakerMembershipSystems': {
                    templateUrl: 'components/experience/templates/rainmakerMembershipSystems.template.html',
                    controller: 'rainmakerMembershipSystemsController'
                }
            }

        })
    })
    .controller('rainmakerMembershipSystemsController', [function () {}]);