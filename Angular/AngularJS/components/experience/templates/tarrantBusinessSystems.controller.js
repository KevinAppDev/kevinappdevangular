﻿angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Experience.TarrantBusinessSystems', {
            url: '/TarrantBusinessSystems',
            views: {
                'viewTarrantBusinessSystems': {
                    templateUrl: 'components/experience/templates/tarrantBusinessSystems.template.html',
                    controller: 'tarrantBusinessSystemsController'
                }
            }

        })
    })
    .controller('tarrantBusinessSystemsController', [function () { }]);