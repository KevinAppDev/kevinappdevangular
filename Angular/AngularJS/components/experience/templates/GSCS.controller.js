﻿angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Experience.GSCS', {
            url: '/GSCS',
            views: {
                'viewGSCS': {
                    templateUrl: 'components/experience/templates/gscs.template.html',
                    controller: 'gscsController'
                }
            }

        })
    })
    .controller('gscsController', [function () {}]);