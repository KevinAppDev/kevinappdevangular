﻿
angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app.Home', {
            url: '/Home',
            templateUrl: "components/home/home.template.html",
            controller: 'homeController'
        })
    })
    .controller('homeController', [function () {

    }]);