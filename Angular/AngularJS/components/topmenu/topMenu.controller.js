﻿
angular.module('kevinappdevApp')
    .config(function ($stateProvider) {
        $stateProvider.state('app', {
            url: '/app',
            templateUrl: 'components/topmenu/topMenu.template.html',
            controller: 'topMenuCtrl'
        })
    })
    .controller('topMenuCtrl', [function () {

    }]);