﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace KevinAppDev.MVC.Models
{
    public class imageDescription
    {
        public string fileName { get; set; }
        public string description { get; set; }
    }
    public class imageDescriptions
    {
        public List<imageDescription> Descriptions;
    }
    public class imageItem
    {
        public string fileName { get; set; }
        public string urlPath { get; set; }
        public long id { get; set; }
        public DateTime fileDate { get; set; }
    }
    public class imageList
    {
       public List<imageItem> images { get; set; }
    }
    public class imageCarouselItem
    {
        public long id { get; set; }
        public string fileName { get; set; }
        public string fileFullPath { get; set; }
        public string urlPathFull { get; set; }
        public string urlPathThumb { get; set; }
        public DateTime fileDate { get; set; }
        public FileAttributes attributes { get; set; }
        public string imageDescription { get; set; }
    }
    public class imageCarouselList
    {
        public string imageSourcePath { get; set; }
        public string imageSourceURLPath { get; set; }
        public string thumbString { get; set; }
        public  imageCarouselList(string _imageSourcePath,string _imageSourceURLPath, string _thumbString)
        {
            this.imageSourcePath = _imageSourcePath;
            this.imageSourceURLPath = _imageSourceURLPath;
            this.thumbString = _thumbString;

            var imageDescriptions = new imageDescriptions();
            imageDescriptions.Descriptions = new List<imageDescription>();
            var descriptionPath = imageSourcePath + "imageDescriptions.json";
            if (System.IO.File.Exists(descriptionPath))
            {
                var d = System.IO.File.ReadAllText(descriptionPath);
                JsonConvert.PopulateObject(d, imageDescriptions);
            }

            images = new List<imageCarouselItem>();
            foreach (var image in Directory.EnumerateFiles(imageSourcePath.Replace(thumbString + "\\","")).Where(fn => !fn.Contains(thumbString)))
            {
                var fi = new FileInfo(image);
                var arr3 = new string[] { ".jpeg", ".jpg"};
                if (arr3.Contains(fi.Extension.ToLower()))
                {
                    imageCarouselItem ii = new imageCarouselItem();
                    var thumbPath = fi.DirectoryName + "\\" + thumbString + "\\" + fi.Name;
                    if (!System.IO.File.Exists(thumbPath))
                    {
                        System.Drawing.Image i = System.Drawing.Image.FromFile(image);
                        System.Drawing.Image resizedImage = i.GetThumbnailImage((100 * i.Width) / i.Height, 100, null, IntPtr.Zero);
                        resizedImage.Save(thumbPath, ImageFormat.Jpeg);
                    }
                    
                    //ii.imageProperty = i.GetPropertyItem(0).Value.ToString();
                    ii.id = images.Count;
                    ii.fileFullPath = image;
                    ii.fileName = Path.GetFileName(image);
                    ii.fileDate = fi.CreationTime;
                    ii.attributes = fi.Attributes;
                    ii.urlPathThumb = imageSourceURLPath + ii.fileName;
                    ii.urlPathFull = ii.urlPathThumb.Replace(thumbString, "");
                    ii.imageDescription = (from iii in imageDescriptions.Descriptions where iii.fileName.ToUpper() == ii.fileName.ToUpper() select iii.description).FirstOrDefault() ?? "";
                    images.Add(ii);
                }
                
            }
        }
        public List<imageCarouselItem> images { get; set; }
        [JsonIgnoreAttribute]
        public string ToJSON
        {
            get { return JsonConvert.SerializeObject(this, Formatting.Indented); ; }
            //set { toJson = value; }
        }
        public string getHTMLCarousel
        {
            get {
                string r = "";
                r = "123";
                r += "456";    

                return r;
            }
        }

    }
}