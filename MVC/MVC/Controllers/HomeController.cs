﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC;


namespace Kevin.MVCSite.Controllers
{
    public class HomeController : BaseController
    {

        public ActionResult Index()
        {
            ViewBag.Title = "Kevin Cornelison";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "About Kevin";
            ViewBag.Message = "MVC online portfolio for Kevin Cornelison.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Kevin Cornelison";

            return View();
        }
        public ActionResult MVC()
        {
            return View();
        }


    }
}