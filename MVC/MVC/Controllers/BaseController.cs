﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using MVC;
using KevinAppDev.MVCSite.Helpers;

namespace Kevin.MVCSite.Controllers
{
   
    public class BaseController : Controller
    {
         public BaseController()
        {
            PageDetailsHelper pd = new PageDetailsHelper(HostingEnvironment.MapPath("~\\App_Data\\PageDetails.json"),"MainCount");

            try
            {
                //ViewBag.Misc = JSONHelpers.ToJson(ModelState);
                //ViewBag.Misc += JSONHelpers.ToJson(ViewBag);
                //ViewBag.Misc += JSONHelpers.ToJson(HttpContext);
                //ViewBag.Misc += (string)this.RouteData.Values["controller"]; 
            }
            catch(Exception ex)
            {
                ViewBag.Misc = ex.ToString();
            }

            ViewBag.PageHitCount = pd.PageHitCount;
            
        }

    }
 

}
