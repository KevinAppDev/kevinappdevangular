﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MVC;

namespace Kevin.MVCSite.Controllers
{
    public class ExperienceController : BaseController
    {
        public ActionResult Experience()
        {
            ViewBag.Title = "Kevin Cornelison Work Experience";
            return View();
        }
        public ActionResult RainmakerMembershipSystems()
        {
            ViewBag.Title = "Rainmaker Membership Systems";
            return View();
        }
        public ActionResult MVTransportation()
        {
            ViewBag.Title = "MV Transit";
            return View();
        }
        public ActionResult GSCS()
        {
            ViewBag.Title = "GSCS";
            return View();
        }
        public ActionResult HeritageAuctionGalleries()
        {
            ViewBag.Title = "Heritage Auction Galleries";
            return View();
        }
        public ActionResult TarrantBusinessSystems()
        {
            ViewBag.Title = "Tarrant Business Systems";
            return View();
        }
        public ActionResult FeaturedSkills()
        {
            ViewBag.Title = "Featured Skills";
            return View();
        }
        public ActionResult FullStackDeveloper()
        {
            ViewBag.Title = "Full Stack Developer";
            return View();
        }
    }
}
