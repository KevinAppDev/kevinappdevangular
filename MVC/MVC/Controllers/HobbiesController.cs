﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using KevinAppDev.MVC.Models;
using Newtonsoft.Json;
using MVC;


namespace Kevin.MVCSite.Controllers
{
    public class HobbiesController : BaseController
    {
        public ActionResult Hobbies()
        {
            ViewBag.Title = "Hobbies";
            return View();
        }
        public ActionResult WoodWorking()
        {
            ViewBag.Title = "Wood Working";
            imageCarouselList icl = new imageCarouselList(Server.MapPath("~/content/images/WoodWorking/thumbs/"), "~/content/images/WoodWorking/thumbs/", "thumbs");
            ViewBag.icl = icl.ToJSON;
            return View(icl);
        }
        public ActionResult Guitar()
        {
            ViewBag.Title = "Guitar";
            return View();
        }
        public ActionResult MicroControllers()
        {
            ViewBag.Title = "Micro Controllers";
            return View();
        }
        public ActionResult Photography()
        {
            ViewBag.Title = "Photography";
            imageCarouselList icl = new imageCarouselList(Server.MapPath("~/content/images/Photography/thumbs/"), "~/content/images/Photography/thumbs/", "thumbs");
            ViewBag.icl = icl.ToJSON;
            return View(icl);
        }
        public ActionResult Electronics()
        {
            ViewBag.Title = "Electronics";
            imageCarouselList icl = new imageCarouselList(Server.MapPath("~/content/images/Electronics/thumbs/"), "~/content/images/Electronics/thumbs/", "thumbs");
            ViewBag.icl = icl.ToJSON;
            return View(icl);
        }
    }
}
