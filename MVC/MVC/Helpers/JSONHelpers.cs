﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KevinAppDev.MVCSite.Helpers
{
    public class JSONHelpers
    {
        public static T FromJson<T>(string contents, T tt)
        {
            Newtonsoft.Json.JsonSerializerSettings jss = new Newtonsoft.Json.JsonSerializerSettings();
            jss.Formatting = Newtonsoft.Json.Formatting.Indented;
            jss.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            jss.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            jss.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
            jss.ConstructorHandling = Newtonsoft.Json.ConstructorHandling.AllowNonPublicDefaultConstructor;
            Newtonsoft.Json.JsonConvert.PopulateObject(contents, tt, jss);
            return tt;
        }
        public static string ToJson(object obj)
        {
            string thisJSON = "";
            try
            {
                Newtonsoft.Json.JsonSerializerSettings jss = new Newtonsoft.Json.JsonSerializerSettings();
                jss.Formatting = Newtonsoft.Json.Formatting.Indented;
                jss.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                jss.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                jss.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
                jss.ConstructorHandling = Newtonsoft.Json.ConstructorHandling.AllowNonPublicDefaultConstructor;
                thisJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj, jss);
            }
            catch (Exception ex)
            {
                thisJSON = ex.ToString();
            }

            return thisJSON;
        }
    }
}