﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KevinAppDev.MVCSite.Helpers
{
    public class PageDetailsHelper
    {
        public PageDetailsHelper(string dataPath, string pageName)
        {
            this.DataPath = dataPath;
            this.PageName = pageName;
            this.LoadPageCount();
        }

        public string  DataPath { get; set; }
        public string PageName { get; set; }
        public long PageHitCount { get; set; }
        List<PageDetail> Details = new List<PageDetail>();
        public void LoadPageCount()
        {
            if (System.IO.File.Exists(DataPath))
            {
                string contents = System.IO.File.ReadAllText(DataPath);
                this.Details.Clear();
                try
                {
                    JSONHelpers.FromJson(contents, this.Details);
                }
                catch
                {
                }

                var pd = (from d in this.Details where d.PageName == this.PageName select d).FirstOrDefault();
                if (pd != null)
                {
                    ++pd.PageCount;
                    pd.LastHitDate = DateTime.Now;
                }
                else
                {
                    pd = new PageDetail();
                    pd.PageCount = 1;
                    pd.PageName = PageName;
                    pd.LastHitDate = DateTime.Now;
                    this.Details.Add(pd);
                }
                this.PageHitCount = pd.PageCount;
                SavePageCount();
            }
        }
        public void SavePageCount()
        {

            string thisJSON = JSONHelpers.ToJson(this.Details.Distinct());
            System.IO.File.WriteAllText(DataPath, thisJSON);
        }



    }
    public class PageDetail
    {
        public string PageName { get; set; }
        public long PageCount { get; set; }
        public DateTime LastHitDate { get; set; }
    }
}