TOOLS:
  Google Canary https://www.google.com/intl/en/chrome/browser/canary.html?brand=KERZ#eula
  Notepad ++ https://notepad-plus-plus.org/
  Atom https://atom.io/
  Packages
    Beautify
  cmder.net http://cmder.net/
  Beyond Compare
  git https://git-scm.com/downloads

FRAMEWORKS:
Android Studio:
  *** MAKE SURE YOU UNINSTALL ANY PREVIOUS VERSIONS FIRST.  This will save you a lot of headache. ***
  Install https://developer.android.com/studio/index.html?gclid=Cj0KEQjw76jGBRDm1K-X_LnrmuEBEiQA8RXYZ92Ng5ykt-dXpLhg2t86tsjOyTh-ZIOIrDCiyF8sIOQaAtm48P8HAQ
  Install the USB driver for the device you want to connect to
  set path=%path%;C:\Users\Slide\AppData\Local\Android\sdk\platform-tools
  Add environment variable ANDROID_HOME=C:\Users\Slide\AppData\Local\Android\sdk1
  Add environment variable ANDROID_SDK_ROOT=C:\Users\Slide\AppData\Local\Android\sdk1
  Run adb devices to get a list of connected devices.
  // Run android SDK manager.
  run C:\Users\Slide\AppData\Local\Android\sdk\tools\android.bat
  // Run android AVD manager.
  run C:\Users\Slide\AppData\Local\Android\sdk\tools\android.bat avd
JAVA:
  Java JDK 1.8 or greater http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
  // Set %JAVA_HOME% = location of JDK 1.8 after install if it does not do this automatically
NODE: v6.10.0
  Node.js http://nodejs.org
  npm install ionic -g
  npm install cordova - g
IONIC: v2.2.1
  // Install ionic
  http://ionicframework.com/docs/v2/intro/installation/
  Create IONIC account https://apps.ionic.io/signup
  // CD to root folder.  A new folder will be created
  // Create a blank IONIC project
  // IONIC start [App Name] [template]
  IONIC start KevinAppDev blank | tabs | Side Menu | Maps

  // CD to [App Name] folder.
  // Will serve the App to a web browser
  IONIC serve

  // Add cordova android / IOS plugins to our project
  IONIC platform add android
  IONIC platform add IOS

  // Build Android version
  IONIC build andoid

  // After sucessfull build you can emulate android or IOS.
  IONIC emulate android
  IONIC emulate IOS

  // Build and install it directly to your connected android device.
  IONIC run android

  IONIC Commands
    IONIC state restore

CORDOVA: V6.5.0
  CORDOVA plugin list
  CORDOVA build
