# README #

This is the main repo for my website.

### What is this repository for? ###

* Shows skillset in MVC and Angular technologies
* Version 1.0.0.1


### How do I get set up? ###

* Pull repo down.
* Load up in Visual Studio.
* Compile.
* Point IIS or some other webservice to MVC folder.

### Contribution guidelines ###

* Writing tests: All endpoints require a unit test.
* Code review: All Pull Requests must be code reviewed.

### Who do I talk to? ###

* Repo owner or admin
* KevinAppDev@gmail.com